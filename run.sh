#!/bin/sh

# TIRA running script
exe_run(){
  java -cp code/target/IRSyestemToucheTask2-1.0-SNAPSHOT-jar-with-dependencies.jar it.unipd.dei.se.hextech.Main "$@" -f
}

run_id=$1

case $run_id in
  1)
    exe_run -boost 1.3 1.2 1.25 -weight 0.4 0.7 1 0 -id "$@"
    ;;
  2)
    exe_run -boost 1.3 1.2 1.25 -weight 0.4 0.7 1 0.6 -id "$@"
    ;;
  3)
    exe_run -boost 1.35 1.25 1.2 -weight 0.6 0.8 1 0.6 -id "$@"
    ;;
  4)
    exe_run -boost 1.3 1.2 1.25 -weight 0.4 0.7 1 0 -id "$@" # with stemmer
    ;;
  5)
    exe_run -boost 1.3 1.2 1.25 -id "$@"
    ;;
esac





