\section{Methodology}
\label{sec:methodology}
The methodology we adopted to build our IR System is based on basic English sentence analysis. \\
We started by making two assumptions: the first one is related to the correct syntax and the correct structure of an English sentence. The second assumption is strictly related to the Touché task we are facing, the argument retrieval for comparative questions. \\
According to the aforementioned assumptions, a document acquires importance if it contains an appropriate number of adjectives, in particular the comparative ones. Indeed, to properly describe or compare different subjects, the sentence has to contain one or more adjectives.
Moreover, the same document is not informative if it contains an elevate number of symbols with respect to the number of words. Instead the document is supposed to be an informative and readable one if it follows the Zipf's least effort principle \cite{zipf}, which states that words of short length are more common than long words.

From the second assumption, being the adjective important in this task, we removed all of them from the stoplist, in order to not lose significative and informative tokens. Moreover we added to the query the synonyms of each adjective and other possible key words.

For the remaining part of the IR System we adopted the usual pipeline offered by the Lucene framework, as can be seen in \autoref{fig:arch}. Now we will describe this architecture in details.

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{figure/arch}
  \caption{A simple view of the offline phase of the system architecture.}
  \label{fig:arch}
\end{figure}

\subsection{System architecture}
We used the Java programming language and the Lucene libraries \cite{Lucene} to implement and to develop our IR System.

The system architecture can be divided in the following stages:

\subsubsection{Parser}
To parse the topics file we used the \texttt{org.w3c.dom} package \cite{orgw3c} of the Java standard library, which has a function to parse the XML file in which the list of topics is stored.

To parse the passages file we used the \text{java.util.zip} package \cite{javazip} from the Java standard library to decompress the gzipped file in real time. It is chained to the Json parsing method offered by the Gson library \cite{Gson}.

\subsubsection{Analyzer}\label{sec:analyzer}
The source of our Analyzer is the standard Tokenizer. Then it maps the returned token stream applying filters in the following order:

\begin{enumerate}
  \item \textbf{Stop filter}: it removes the most common English words that are not  informative \cite{stopfilter}. This filter works in a case insensitive mode to remove also capitalized and case mistyped words. Moreover it does not remove the adjectives, as stated in \autoref{sec:methodology}.
  \item \textbf{Lowercase or brand filter}: for what concerns letter case, we convert all the tokens to lowercase in order to match the same token in all possible writing combinations.
    In fact, normally, brand and product names can be converted to lowercase without losing information. However, there is an exception for all the words which refer to famous brands  \cite{brandnames}, and at the same time to common English words \cite{wordlist} (e.g. Apple the brand and apple the fruit).
    In this exceptional case we do not perform any token modification. It is important for this task to preserve brand names information, since a lot of queries involves comparison between brands or products.
  \item \textbf{Lowercase copy filter}: it integrates the previous filter taking care of initial capitals and possible writing errors, which could be misled with product names.
    To overcome these situations, for each non-lowercase token\footnote{Thanks to the previous filter, they refer to both words and names.}, we duplicate the non-lowercase tokens: one copy will remain as the original, the other will become lowercase. Finally we obtain a token for the name and a token for the word.

    The last two filters were written in a separated way to possibly allow the insertion of grammatical analysis filter. This is required since the token duplication alters the structure of the sentences.
  \item \textbf{Lovins stem filter}: this is a famous stem filter, designed by Julie B. Lovins, which produces words stripped by their suffixes \cite{lovins}.
\end{enumerate}

\subsubsection{Indexer}\label{sec:indexer}
We used the standard Lucene Indexer with the BM25 similarity \cite{bm25_1,bm25_2}.

\smallskip
During the indexing phase, for each document the quality score is computed (as said in \autoref{sec:introduction}).
During the search phase, the quality score is multiplied to the query score, with the objective of re-ranking the retrieved documents.
The aim is to penalize bad written documents and to promote comparative passages with respect to descriptive ones.

The quality is internally represented by a convex combination of the following frequencies:
\begin{description}
  \item[Symbol frequency] A document with plenty of not informative symbols (e.g. \#, emoji, \dots) is penalized because it could be a bad written passage, and it usually refers to click bait, scam or promotional pages.\\Some symbols brings no penalty if they are in an appropriate quantity (e.g. !, ?, \dots): the penalty of each symbol is computed as $1 - 1/n$ where $n$ is the $n^{\text{th}}$ occurrence of the symbol.
  % \item[Phrases repetitions] A passage with too many phrases repetitions is generally not so good and must be penalized.
  \item[Words length frequency] To assure that the document follows the Zipf's least effort principle, we compute the difference between the short words frequency and the long words one. We consider as short tokens the words of length less or equal than four. Finally we rescale the aforementioned difference between 0 and 1.
  \item[Adjective frequency] A document, in order to be descriptive or comparative, it must contain adjectives. To capture this property we compute the frequency of adjectives with respect to the total number of words. Intuitively, the higher the frequency, the more descriptive is the document.
  \item[Comparative adjective frequency] As the same reasoning of the previous frequency, the higher the frequency, the more descriptive is the document.
    To achieve this we compute the ratio between the number of comparative adjective divided by the total number of adjectives.
  % \item[Frequency difference of the two most frequent words] If there is a predominant word the document is likely to be descriptive of one thing, besides if there are two predominant words the passage is supposed to be a comparison.
\end{description}

The last two contributions have a lighter weights in the convex combination, in fact these frequencies distinguish between two types of good documents, preferring comparative ones.

Eventually, we add a bias to the convex combination as form of smoothing, to avoid bringing final scores to zero.

\subsubsection{Searcher}
A part from the aforementioned re-ranking (see \autoref{sec:introduction}) by document quality, we used the standard Lucene IndexSearcher with the same configuration of the Indexer.

For what concerns relevance evaluation, we used the boolean model to build a single query composed by the following sub-queries, each appropriately boosted and using the boolean clause \emph{should} (logical or):

\begin{enumerate}
  \item The first sub-query contains all the terms returned by the same Analyzer used in the Indexer (\autoref{sec:analyzer}). We assigned the highest boost to this query because it represents the user information need.
  \item In the second sub-query we expand the previous one adding the synonyms of the terms, taken from a list \cite{synonims}.
  \item The last sub-query contains only the N-grams detected by the POS analyzer.
\end{enumerate}

Even if for each topic we return 1\,000 ranked documents, we decided to retrieve 10 times that number, because when we multiply the query score by the quality one, there is the possibility that some documents ranked at a position greater than 1\,000 would climb the ranking. Without this expedient, those documents would not be considered at all.


